# Trustable Distros Overview

This is a WIP project to establish foundations for operating systems
(or distributions) which could satisfy the broad goals stated in the original
call to action for the
[trustable software engineering project](https://trustable.gitlab.io).

This project and its repository will deal with context and metadata for the
work, including:

- documentation about [contributors](contributors.md), processes,
  [intents](intents.md), requirements, and infrastructure
- issues and tasks tracking
- links to code, tests, artifacts and results