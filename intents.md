# Intents

As described in the (work-in-progress) definitions for the trustable software
project, this is our initial document covering overall needs/objectives for the
trustable distros project from the stakeholder perspective.

The call to action and associated approvals were conducted on the
[trustable software engineering list](https://lists.trustable.io/pipermail/trustable-software/2018-July/thread.html#387)

We are aiming for:

- documented processes and tooling for delivery of Operating System (OS)
  software for use in critical real-world/production scenarios
- documentation of critical behaviours which can be claimed to be guaranteed for
  specific implementations on hardware, along with tests and evidence to support
  the guarantees
- CI/CD infrastructure for ongoing validation and deployment of changes and
  upgrades for long-term installations (ten years or more)
- mechanism for downstream users/installations to consume planned and unplanned
  updates (including urgent security fixes) reliably over the long term
- establishment of a workable approach for independent validation of the OS
  against applicable standards e.g. IEC 61508, others to be defined
- evidence to support certification of the OS by applicable standards authorities
  in regulated industries such as finance, automotive and power
- viable mechanism for re-validation and re-certification of updated versions
  of the OS

## Operating System: definition and approach to release and maintenance

A candidate operating system (OS) satisfies the following:

- OS governance includes
  - guarantees to satisfy stated uptime, latency, performance and/or other
    criteria, on example hardware
  - guarantees to provide defined ABI services on example hardware
  - if multiple concurrent threads/processes/applications/guests are supported,
    guarantees to provide isolation of resources for each, and guarantees for
    behaviour of shared resources and/or libraries if applicable
  - a mechanism to bootstrap the OS and toolchain on new hardware
  - a hermetic build environment; the OS is constructed from a controlled set
    of known inputs
  - a mechanism for ensuring that the OS is constructed deterministically
    from source code (bit-for-bit reproducible)

- the OS is defined as:
  - a set of components (package list) validated to provide services on example
    hardware and/or virtual machines
  - a toolchain for construction of compatible software which is bit-for-bit
    reproducible
  - a mechanism to provide upgrades for consumers of the OS such that:
      - security updates can be applied rapidly with minimal risk
      - documentation for updates is comprehensive and easy to understand
  - an ongoing sequence of releases such that
      - each release is frequently updated and such updates do not break ABI
      - occasionally a following release is offered with updated ABI
      - releases with old ABIs will cease to receive updates two years after
        the announcement of the following release

- for each component in the package list:
  - the origin is known and documented
  - the outputs from the known origin are collected in version control, with
    history
  - history is also maintained for any modifications required for
    OS-specific integration
  - any attempts to re-write received history are identified and treated as
    possible attacks
